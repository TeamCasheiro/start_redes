#!/bin/bash
echo "Configurando Roteador do servidor . . ." 
echo "."
echo ". ."
echo ". . ."
echo ". . . ."
ifconfig eth0 160.0.0.1
ifconfig eth1 200.17.200.1 netmask 255.255.255.252
ifconfig eth2 120.1.2.1 netmask 255.255.255.252
ifconfig eth3 150.0.0.1 netmask 255.255.255.252
exit
