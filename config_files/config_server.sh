#!/bin/bash
echo "Configurando Servidor . . ." 
echo "."
echo ". ."
echo ". . ."
echo ". . . ."
ifconfig eth0 160.0.0.3


echo "Configurando Rota . . ."
echo "."
echo ". ."
echo ". . ."
echo ". . . ."
route add default gw 160.0.0.1

echo "Configurando servidor para escutar na porta 5151 em segundo plano. . ."
echo "."
echo ". ."
echo ". . ."
echo ". . . ."
ncat -l -p 5151 -k &
echo "finalizado."
exit