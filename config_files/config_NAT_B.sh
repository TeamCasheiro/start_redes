#!/bin/bash
echo "Configurando Roteador e NAT para client B . . ." 
echo "."
echo ". ."
echo ". . ."
echo ". . . ."
ifconfig eth0 192.168.0.1
ifconfig eth1 120.1.2.2 netmask 255.255.255.252


echo "Configurando Rota . . ."
echo "."
echo ". ."
echo ". . ."
echo ". . . ."
route add default gw 120.1.2.1
echo "Mascarando IP . . ."
echo "."
echo ". ."
echo ". . ."
echo ". . . ."
iptables -t nat -A POSTROUTING -o eth1 -j MASQUERADE

exit