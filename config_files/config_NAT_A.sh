#!/bin/bash
echo "Configurando Roteador e NAT para client A . . ." 
echo "."
echo ". ."
echo ". . ."
echo ". . . ."
ifconfig eth0 192.168.0.1
ifconfig eth1 200.17.200.2 netmask 255.255.255.252


echo "Configurando Rota . . ."
echo "."
echo ". ."
echo ". . ."
echo ". . . ."
route add default gw 200.17.200.1
echo "MAscarando IP . . ."
echo "."
echo ". ."
echo ". . ."
echo ". . . ."
iptables -t nat -A POSTROUTING -o eth1 -j MASQUERADE

exit