# README #

### Implementar utilizando qualquer linguagem de programa��o DUAS aplica��es que utilizam TCP: ###

- Uma aplica��o para rodar em um host quem � conhecido por todos os clientes. Essa aplica��o ser� chamada de Gerenciador (escuta na porta 5151).
- Uma aplica��o chamada FClient que ser� instalada em diversos hosts e que estabelecer� inicialmente uma conex�o com o Gerenciador para conhecer os outros hosts FClient.
- As duas aplica��es devem ser implementadas utilizando a API Socket.

### Objetivo: ###
- Um host com FClient ir� solicitar o download de um arquivo de outro host FClient atrav�s do Gerenciador.


### Modo de opera��o: ###
* Cada FClient se registra em um servidor informando:
    * Seu endere�o IP externo
    * Sua porta de origem
    * Sua lista de arquivos locais
* Um FClient solicita ao Gerenciador a lista de outros FClient
* Um FClient solicita ao Gerenciador a lista de arquivos de um FClient qualquer.
* Um FClient X solicita o download de um arquivo do FClient Y, da seguinte forma:

* FClient X solicita arquivo ao Gerenciador
* Gerenciador avisa FClient Y da solicita��o do FClient X
* FClient Y tenta iniciar uma nova conex�o TCP com FClient X.
* FClient Y avisa o Gerenciador sobre o status da conex�o:
    * Funcional: Y conseguiu abrir conex�o com X
    * Problema: Y n�o conseguiu abrir uma conex�o com X
#### Em caso de Problema dever� ocorrer uma tentativa de X para Y. ####
* Avisar novamente o Gerenciador do status da conex�o:
    * Funcional: X conseguiu abrir uma conex�o com Y
    * DualProblema: X tamb�m n�o conseguiu abrir uma conex�o com Y.
#### Em caso de DualProblema avisar os FClient. ####

* Ap�s a aberta a conex�o entre os FClient dever� ocorrer a transferencia de arquivo.
* O arquivo transferido deve ser salvo com o nome FClient_<ID>_<timestamp>: 
    * Onde ID � um valor que identifica um FClient no Gerenciador e timestamp � o hor�rio de inicio da transfer�ncia.
* O novo arquivo deve ser informado ao Gerenciador.


#### Avalia��o e Testes: ####
* Desenhar uma m�quina finita de estados (Finite State Machine) com todos os estados poss�veis do Gerenciador e dos FClient.
* Um ambiente de teste deve ser montado para validar a solu��o.
* Esse ambiente pode ser criado no GNS3.
* Instalar FClient em hosts que funcionam atrav�s de NAT.
* Testar todos os casos (Funcional, Problema, DualProblema)

#### Equipes: ####
- M�nimo de 2 e m�ximo de 3 alunos por equipe.

#### Entrega: ####
- Defesa do trabalho parcial ou completo nos dias:

    * 08/Nov
    * 13/Nov
    * 20/Nov
    * 22/Nov
    * 29/Nov - Ultimo dia letivo
- As defesas ocorrer�o no laborat�rio, no hor�rio da aula.
- Valor 0 a 100
- As notas ser�o individuais e n�o por equipe
- Agendar defesa em: https://teamup.com/ksmr9pdhmqahvyvgjt

##### N�o alterar hor�rios j� agendados. #####
##### Incluir o nome dos membros da equipe #####
##### Considerar defesa de 20m/cada. #####

- Numero m�ximo de defesas por aula: 5 #####

# CONFIGURA��ES #

### Para configurar a appliance pelo docker ###

* docker run -it --entrypoint=/bin/bash gns3/ubuntu:full2 -i
de um update depois instale o python e o openssh-server o man e o nmap
* apt install python openssh-server man nmap
adicione um usuario qualquer e uma senha
* adduser tads

### verifique se esta tudo certo com o ssh ###

* /etc/init.d/ssh start

### Para enviar arquivos para o appliance ###

* scp <nomeDoArquivo> <user>@<number_ip>:<destino(opcional, default enviado para home)>
* scp arquivo.txt tads@172.17.0.2:
apos sair do container e necessario commita-lo para que se torne uma imagem
* docker ps -a
copie os dados do container desejado
* docker commit 39cf6d62398a        gns3/ubuntu:full2
	
### What is this repository for? ###

* Quick summary
* Version
* [Learn Markdown](https://bitbucket.org/tutorials/markdowndemo)

### How do I get set up? ###

* Summary of set up
* Configuration
* Dependencies
* Database configuration
* How to run tests
* Deployment instructions

### Contribution guidelines ###

* Writing tests
* Code review
* Other guidelines

### Who do I talk to? ###

* Repo owner or admin
* Other community or team contact
